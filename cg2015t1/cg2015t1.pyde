#Primeiro Trabalho de Computação Gráfica
#Bruno Tourinho Tomas - 109041274
#Prof Cláudio Esperança

#Definicoes iniciais
#Lista que armazena os segmentos de reta
retas = []

#Pontos
r1=[100,100]
r2=[500,500]
s1=[100,500]
s2=[500,100]

#Segmentos de reta
retas+=[r1+r2]
retas+=[s1+s2]

#Marca qual dos 4 pontos foi clicado, [-1,-1] no caso contrário.
point = [-1,-1]

#Tratamento de igualdade entre floats (a, b) com tolerância de "delta"
def floatEquals(a, b, delta):
    if abs(a-b) < delta: 
        return True
    else: 
        return False
#Calcula o coeficiente de uma reta com pontos (x0, y0) e (x1, y1)
#Retorna o vetor [a, b] com os coeficientes angular e linear da reta, respectivamente
def coefReta(x0, y0, x1, y1):
    if x0 == x1:
        a = 0
    else:
        a = (float(y0)-y1)/(float(x0)-x1)
    b = y0 - (x0 * a)
    return [a,b]
#Calcula o ponto de interseção entre as retas r e s, 
#expressas em seus coeficientes angular e linear [a, b]
def intersecao(r, s):
    if r[0]==s[0]:
        return "paralelas"
    else:
        x = -(r[1]-s[1])/(r[0]-s[0])
        y = (r[0]*x) + r[1]
    return [x,y]
#Verifica se um ponto (xp, yp) é interno a um segmento de reta
#de coordenadas das extremidades (x0, y0) e (x1, y1)
def pontoInterno(x0, y0, x1, y1, xp, yp):
    d1 = dist(x0, y0, xp, yp)
    d2 = dist(x1, y1, xp, yp)
    d3 = dist(x0, y0, x1, y1)
    if floatEquals(d3,d1+d2, 0.001):
        return True
    else:
        return False
    
def setup():
    size(600,600)
    
def mousePressed():
    for coord in retas:
        d1 = coord[:2]+[mouseX, mouseY]
        d2 = coord[2:]+[mouseX, mouseY]
        if dist(*d1) < 10:
            point[0]=retas.index(coord)
            point[1]=0
        elif dist(*d2) < 10:
            point[0]=retas.index(coord)
            point[1]=1

def mouseDragged():
    if -1 not in point:
        if point[1] == 0:
            retas[point[0]][:2]=[mouseX, mouseY]
        elif point[1] == 1:
            retas[point[0]][2:]=[mouseX, mouseY]
            
def mouseReleased():
    point[0] = -1
    point[1] = -1

def draw():
    background(200)
    for coord in retas:
        p1=coord[:2]+[10,10]
        p2=coord[2:]+[10,10]
        line(*coord)
        ellipse(*p1)
        ellipse(*p2)
    r = coefReta(*retas[0])
    s = coefReta(*retas[1])
    intersec = intersecao(r,s)
    pontosReta1 = retas[0] + intersec
    pontosReta2 = retas[1] + intersec
    if pontoInterno(*pontosReta1) and pontoInterno(*pontosReta2):
        ponto = intersec+[10, 10]
        fill(255,0,0)
        ellipse(*ponto)
        fill(255,255,255)
    #Imprime o ponto de encontro para efeitos de debugging
    #print point 
